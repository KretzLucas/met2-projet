DROP DATABASE IF EXISTS Commerce;
CREATE DATABASE Commerce;

DROP USER IF EXISTS 'user'@'localhost';
CREATE USER 'user'@'localhost' IDENTIFIED WITH mysql_native_password BY '';
GRANT ALL ON Commerce.* TO 'user'@'localhost';

USE Commerce;

CREATE TABLE Utilisateurs
(
    id INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100) NOT NULL,
    prenom VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    telephone VARCHAR(15) NOT NULL,
    motDePasse VARCHAR(50) NOT NULL,
    confirmation VARCHAR(50) NOT NULL,
    CONSTRAINT PK_Utilisateurs PRIMARY KEY (id)
);

CREATE TABLE Combattants
(
    id INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100) NOT NULL,
    prenom VARCHAR(100) NOT NULL,
    naissance INT NOT NULL,
    poids INT NOT NULL,
    taille FLOAT NOT NULL,
    sport VARCHAR(20) NOT NULL,
    pointFort VARCHAR(40) NOT NULL,
    image VARCHAR(200) NOT NULL,
    prix FLOAT NOT NULL,
    CONSTRAINT PK_Combattants PRIMARY KEY (id)
);

INSERT INTO Utilisateurs VALUES (NULL, "Son", "Goku", "goku.son@gmail.com", "+33611223344", "JsLsLpPdLt", "JsLsLpPdLt");
INSERT INTO Utilisateurs VALUES (NULL, "Son", "Gohan", "gohan.son@gmail.com", "+33611223355", "JsSsLpPdLt", "JsSsLpPdLt");
INSERT INTO Utilisateurs VALUES (NULL, "Son", "Goten", "goten.son@gmail.com", "+33611223366", "JsTsLpPdLt", "JsTsLpPdLt");
INSERT INTO Utilisateurs VALUES (NULL, "Kretz", "Lucas", "lucas.kretz67@gmail.com", "+33640576426", "TestTest", "TestTest");
INSERT INTO Utilisateurs VALUES (NULL, "Test", "Test", "Test@gmail", "+33999999999", "TestTest", "TestTest");

INSERT INTO Combattants VALUES (NULL, "Tyson", "Mike", 1966, 109, 1.78, "Boxe Anglaise", "Uppercut droit", "http://img.over-blog-kiwi.com/1/18/57/55/20180127/ob_b85fb9_4-copie.jpg", 559);
INSERT INTO Combattants VALUES (NULL, "Le Banner", "Jérôme", 1972, 119, 1.9, "Kick-Boxing", "Coup de pied droit", "http://fr.web.img6.acsta.net/r_1920_1080/medias/nmedia/18/63/21/56/18879113.jpg", 459);
INSERT INTO Combattants VALUES (NULL, "McGregor", "Conor", 1988, 70, 1.75, "MMA", "Knee-kick", "https://www.borntoworkout.com/wp-content/uploads/2019/06/Conor-McGregor-Workout.jpg", 439);
INSERT INTO Combattants VALUES (NULL, "Joshua", "Anthony", 1989, 108, 1.98, "Boxe Anglaise", "Résistant", "https://i.pinimg.com/474x/0b/92/58/0b925816e32842b4086440f7f57917be.jpg", 400);
INSERT INTO Combattants VALUES (NULL, "Lee", "Bruce", 1940, 62, 1.72, "Kung Fu", "One-Inch Punch", "https://i.pinimg.com/474x/8d/07/37/8d07378770fcff4292aa647eecdf28c4.jpg", 600);
INSERT INTO Combattants VALUES (NULL, "Norris", "Chuck", 1940, 73, 1.73, "Karaté", "6ème sens", "https://i.pinimg.com/474x/25/28/3d/25283d6f2079038865c1efda1c5c8716--chuck-norris-movies-karate-karate.jpg", 1000);
INSERT INTO Combattants VALUES (NULL, "Mathers", "Marshall", 1972, 68, 1.73, "Battle rap", "Clash", "https://i.pinimg.com/474x/48/20/0c/48200c82f51dec8ef7836cd2d7de36e9.jpg", 850);
INSERT INTO Combattants VALUES (NULL, "Goku", "Son", 737, 62, 1.75, "Secret", "Kamehameha", "https://pbs.twimg.com/media/DaCSte_WAAATznF.jpg", 1500);
INSERT INTO Combattants VALUES (NULL, "Lodbrok", "Ragnar", 750, 86, 1.83, "Vikings", "Stratégie", "https://i.pinimg.com/474x/f5/c7/7f/f5c77f9c62fef8eb1fe6f7201814877a.jpg", 900);