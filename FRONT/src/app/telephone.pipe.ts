import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'telephone'
})
export class TelephonePipe implements PipeTransform {

  transform(args: string): string {
    let newValue: string;

    if (args.substring(0, 1) == "0") {
        newValue = "+33" + args.substring(1);
    }
    else {
        newValue = args;
    }

    return newValue;
  }

}
