import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Combattant } from './model/combattant/combattant';
import { environment } from 'src/environments/environment';
import { Utilisateur } from './model/utilisateur/utilisateur';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class EchangeService {  
  utilisateurs: Array<Utilisateur> = new Array();
  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { 
    this.GetUtilisateurs().subscribe(utilisateur => this.utilisateurs = utilisateur);
  }

  public GetCombattants(): Observable<Combattant[]> {
    this.UpdateJwt();
    return this.http.get<Combattant[]>(environment.listeCombattants, this.httpOptions);
  }

  public GetCombattant(id: number): Observable<Combattant> {
    this.UpdateJwt();
    return this.http.get<Combattant>(environment.listeCombattants + "/" + id.toString(), this.httpOptions);
  }

  public GetUtilisateurs(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(environment.listeUtilisateurs);
  }

  public GetUtilisateur(id: number): Observable<Utilisateur> {
    return this.http.get<Utilisateur>(environment.listeUtilisateurs + "/" + id.toString());
  }

  public Login(email: string, motDePasse: string): Observable<any> {    
    return this.http.post(environment.login, JSON.stringify({ email: email , motDePasse: motDePasse }), this.httpOptions);
  }

  public CreerUtilisateur(utilisateur: Utilisateur) {
    return this.http.post(environment.creerUtilisateur, utilisateur, this.httpOptions);
  }

  public Deconnexion() {
    sessionStorage.setItem('token', null);
  }

  public EstConnecte(): boolean {
    let res: boolean = false;

    if(sessionStorage.getItem('token') != "null" && !this.TokenExipire()) {
      res = true;
    } 

    return res;
  }

  public TokenExipire(): boolean {
    let helper = new JwtHelperService();

    return helper.isTokenExpired(sessionStorage.getItem('token'));
  }

  public UpdateJwt() {
    this.httpOptions = {
      headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('token')
      })
    }
  }
}
