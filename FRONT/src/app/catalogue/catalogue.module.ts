import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CatalogueRoutingModule } from './catalogue-routing.module';
import { CatalogueComponent } from './catalogue.component';
import { ProduitComponent } from './produit/produit.component';
import { DetailComponent } from './produit/detail/detail.component';
import { FiltreProduitPipe } from '../filtre-produit.pipe';

@NgModule({
  declarations: [
    CatalogueComponent,
    ProduitComponent,
    DetailComponent,
    FiltreProduitPipe
  ],
  imports: [
    CommonModule,
    CatalogueRoutingModule,
    FormsModule
  ]
})
export class CatalogueModule { }
