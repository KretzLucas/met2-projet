import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { CombattantActionAdd } from 'src/app/model/combattant/combattant-action-add';
import { Combattant } from 'src/app/model/combattant/combattant';
import { Observable } from 'rxjs';
import { EchangeService } from 'src/app/echange.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.less']
})
export class ProduitComponent implements OnInit {
  @Input() barreRecherche: string;
  combattants: Observable<Combattant[]>;

  constructor(private echangeService: EchangeService, private store: Store, private router: Router) { }

  ngOnInit() {
    if(!this.echangeService.EstConnecte()) {
      this.router.navigate(['/Connexion']);
    }
    else {
      this.combattants = this.echangeService.GetCombattants();
    }
  }

  AjouterAuPanier(combatant: Combattant) {
    this.AddCombattant(combatant);
  }

  AddCombattant(combattant) {
    this.store.dispatch(new CombattantActionAdd(combattant));
  }
}
