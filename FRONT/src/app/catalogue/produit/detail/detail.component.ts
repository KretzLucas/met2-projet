import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Combattant } from 'src/app/model/combattant/combattant';
import { ActivatedRoute, Router } from '@angular/router';
import { EchangeService } from 'src/app/echange.service';
import { Store } from '@ngxs/store';
import { CombattantActionAdd } from 'src/app/model/combattant/combattant-action-add';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.less']
})
export class DetailComponent implements OnInit {
  combattant: Observable<Combattant>;
  
  constructor(private route: ActivatedRoute, private echangeService: EchangeService, private store: Store, private router: Router) { }

  ngOnInit() {
    if(!this.echangeService.EstConnecte()) {
      this.router.navigate(['/Connexion']);
    }
    else {
      this.combattant = this.echangeService.GetCombattant(+this.route.snapshot.paramMap.get('id'));
    }
  }

  AjouterAuPanier(combatant: Combattant) {
    this.AddCombattant(combatant);
  }

  AddCombattant(combattant) {
    this.store.dispatch(new CombattantActionAdd(combattant));
  }

}
