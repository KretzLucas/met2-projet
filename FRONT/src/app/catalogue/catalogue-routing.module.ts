import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogueComponent } from './catalogue.component';
import { ProduitComponent } from './produit/produit.component';
import { DetailComponent } from './produit/detail/detail.component';

const routes: Routes = [
  {path:'', component:CatalogueComponent},
  {path:'Produit', component:ProduitComponent},
  {path:'Produit/Detail/:id', component:DetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogueRoutingModule { }
