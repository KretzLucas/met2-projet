import { Component, OnInit } from '@angular/core';
import { EchangeService } from '../echange.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.less']
})
export class CatalogueComponent implements OnInit {

  constructor(private echangeService: EchangeService, private router: Router) { }

  ngOnInit() {
    if(!this.echangeService.EstConnecte()) {
      this.router.navigate(['/Connexion']);
    }
  }

}
