import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtreProduit'
})
export class FiltreProduitPipe implements PipeTransform {

  transform(items: any[], searchText: string): any {
    if (!items) {
        return [];
    }
    if (!searchText) {
        return items;
    }

    searchText = searchText.toLowerCase();
    console.log(items);
    return items.filter(it => {
      console.log(it);
        return it.nom.toLowerCase().includes(searchText)
            || it.prenom.toLowerCase().includes(searchText)
            || it.naissance.toString().toLowerCase().includes(searchText)
            || it.sport.toLowerCase().includes(searchText);
    });
  }

}
