import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Combattant } from '../model/combattant/combattant';
import { Store } from '@ngxs/store';
import { CombattantActionDel } from '../model/combattant/combattant-action-del';
import { EchangeService } from '../echange.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.less']
})
export class PanierComponent implements OnInit {
  combattants: Observable<Combattant>;

  constructor(private store: Store, private echangeService: EchangeService, private router: Router) { 
    this.combattants = this.store.select(state => state.combattant.combattant);
  }

  ngOnInit() {
    if(!this.echangeService.EstConnecte()) {
      this.router.navigate(['/Connexion']);
    }
  }

  EnleverDuPanier(combattant: Combattant) {
    this.DelCombattant(combattant);
  }

  DelCombattant(combattant) { 
    this.store.dispatch(new CombattantActionDel(combattant)); 
  }

  Payement() {
      alert("Votre paiement a bien été pris en compte et sera traité par nos équipes dans les meilleurs délais !");
  }

}
