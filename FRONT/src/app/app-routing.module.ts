import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'', loadChildren: () => import('./connexion/connexion.module').then(m=>m.ConnexionModule)},
  {path:'Acceuil', loadChildren: () => import('./catalogue/catalogue.module').then(m=> m.CatalogueModule)},
  {path:'Catalogue', loadChildren: () => import('./catalogue/catalogue.module').then(m=> m.CatalogueModule)},
  {path:'Panier', loadChildren: () => import('./panier/panier.module').then(m=> m.PanierModule)},
  {path:'Connexion', loadChildren: () => import('./connexion/connexion.module').then(m=>m.ConnexionModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
