import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { EchangeService } from '../echange.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  nbrCombattants: number = 0;

  constructor(private store: Store, private echangeService: EchangeService, private router: Router) { 
    this.store.select(state => state.combattant.combattant).subscribe(u => this.nbrCombattants = u.length);
  }

  ngOnInit() {
  }

  EstConnecte() {
    return this.echangeService.EstConnecte();
  }

  Deconnexion(){
    this.echangeService.Deconnexion();
    this.router.navigate(['/Connexion']);
  }

  PanierRempli() {
    let res: boolean = false;
    
    if(this.nbrCombattants > 0) {
      res = true;
    }

    return res;
  }
}
