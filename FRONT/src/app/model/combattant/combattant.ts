export class Combattant {
    id: number;
    nom: string;
    prenom: string;
    naissance: number;
    poids: number;
    taille: number;
    sport: string;
    pointFort: string;
    image: string;
    prix: number;

    constructor() {
        
    }
}
