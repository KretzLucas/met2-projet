import { Combattant } from './combattant';

export class CombattantActionDel {
    static readonly type = '[Combattant] Del';

    constructor(public payload: Combattant) {
        
    }
}
