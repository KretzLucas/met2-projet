import { Combattant } from './combattant';

export class CombattantActionAdd {
    static readonly type = '[Combattant] Add';

    constructor(public payload: Combattant) {

    }
}
