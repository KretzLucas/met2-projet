import { State, Selector, Action, StateContext } from '@ngxs/store';
import { CombattantStateModel } from './combattant-state-model';
import { CombattantActionAdd } from './combattant-action-add';
import { CombattantActionDel } from './combattant-action-del';

@State<CombattantStateModel>({
    name: 'combattant',
    defaults: {
        combattant: []
    }
})

export class CombattantState {
    id: number = 0;

    @Selector()
        static GetCombattant(state: CombattantStateModel) {
            return state.combattant;
        }

    @Action(CombattantActionAdd)
        Add({ getState, patchState }: StateContext<CombattantStateModel>, { payload }: CombattantActionAdd) {
            const state = getState();
            patchState({
                combattant: [...state.combattant, {...payload, id: this.id}]
            });
            this.id++;
        }

    @Action(CombattantActionDel)
        Del({ getState, patchState }: StateContext<CombattantStateModel>, { payload }: CombattantActionDel) {
            const state = getState();
            patchState({
                combattant: [...(state.combattant.slice(0, state.combattant.indexOf(payload))), ...(state.combattant.slice(state.combattant.indexOf(payload) + 1))]
            })
        }
}
