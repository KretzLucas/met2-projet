import { Utilisateur } from './utilisateur';

export class UtilisateurActionAdd {
    static readonly type = '[Utilisateur] Add';

    constructor(public payload: Utilisateur) {
        
    }
}
