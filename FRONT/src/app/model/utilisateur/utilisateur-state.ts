import { State, Selector, Action, StateContext } from '@ngxs/store';
import { UtilisateurStateModel } from './utilisateur-state-model';
import { UtilisateurActionAdd } from './utilisateur-action-add';

@State<UtilisateurStateModel>({
    name: 'utilisateur',
    defaults: {
        utilisateur: []
    }
})

export class UtilisateurState {
    @Selector()
        static GetUtilisateur(state: UtilisateurStateModel) {
            return state.utilisateur;
        }
    
    @Action(UtilisateurActionAdd)
        Add({ getState, patchState }: StateContext<UtilisateurStateModel>, { payload }: UtilisateurActionAdd) {
            const state = getState();
            patchState({
                utilisateur: [...state.utilisateur, payload]
            });
        }
}
