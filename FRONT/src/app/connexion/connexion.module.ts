import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConnexionRoutingModule } from './connexion-routing.module';
import { ConnexionComponent } from './connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { RecapitulatifComponent } from './inscription/recapitulatif/recapitulatif.component';
import { ErrorDirective } from '../error.directive';
import { TelephonePipe } from '../telephone.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ConnexionComponent,
    InscriptionComponent,
    RecapitulatifComponent,
    TelephonePipe,
    ErrorDirective
  ],
  imports: [
    CommonModule,
    ConnexionRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    TelephonePipe
  ]
})
export class ConnexionModule { }
