import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/model/utilisateur/utilisateur';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-recapitulatif',
  templateUrl: './recapitulatif.component.html',
  styleUrls: ['./recapitulatif.component.less']
})
export class RecapitulatifComponent implements OnInit {
  utilisateur: Observable<Utilisateur>;

  constructor(private store: Store) { 
  }
  

  ngOnInit() {
    this.utilisateur = this.store.select(state => state.utilisateur.utilisateur);
  }

}
