import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Utilisateur } from 'src/app/model/utilisateur/utilisateur';
import { Router } from '@angular/router';
import { TelephonePipe } from 'src/app/telephone.pipe';
import { EchangeService } from 'src/app/echange.service';
import { Store } from '@ngxs/store';
import { UtilisateurActionAdd } from 'src/app/model/utilisateur/utilisateur-action-add';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.less']
})
export class InscriptionComponent implements OnInit {
  inscriptionForm: FormGroup;
  utilisateur = new Utilisateur();
  reponse: any;

  constructor(private formBuilder: FormBuilder, private telephonePipe: TelephonePipe, private router: Router, private echangeService: EchangeService, private store: Store) { 
    this.inscriptionForm = this.formBuilder.group({
      inputNom: ['', [Validators.required, Validators.pattern("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$")]],
      inputPrenom: ['', [Validators.required, Validators.pattern("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$")]],
      inputEmail: ['', [Validators.required, Validators.pattern("[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})")]],
      inputTelephone: ['', [Validators.required, Validators.pattern("^(0|\\+33)[1-9]([-. ]?[0-9]{2}){4}$")]],
      inputMotDePasse: ['', [Validators.required, Validators.minLength(8)]],
      inputConfirmation: ['', [Validators.required, Validators.minLength(8)]]
  });
  }

  ngOnInit() {
  }

  TestNom(): boolean {
    let valide: boolean = false;

    if (this.inscriptionForm.controls.inputNom.valid) {
        valide = true;
    }

    return valide;
  }

  TestPrenom(): boolean {
      let valide: boolean = false;

      if (this.inscriptionForm.controls.inputPrenom.valid) {
          valide = true;
      }

      return valide;
  }

  TestEmail(): boolean {
    let valide: boolean = false;

    if (this.inscriptionForm.controls.inputEmail.valid) {
        valide = true;
    }

    return valide;
  }

  TestTelephone(): boolean {
      let valide: boolean = false;

      if (this.inscriptionForm.controls.inputTelephone.valid) {
          valide = true;
      }

      return valide;
  }

  TestMotDePasse(): boolean {
      let valide: boolean = false;

      if (this.inscriptionForm.controls.inputMotDePasse.valid) {
          valide = true;
      }

      return valide;
  }

  TestConfirmation(): boolean {
      let valide: boolean = false;

      if (this.inscriptionForm.controls.inputConfirmation.value == this.inscriptionForm.controls.inputMotDePasse.value) {
          valide = true;
      }

      return valide;
  }

  AddUtilisateur(utilisateur) { this.store.dispatch(new UtilisateurActionAdd(utilisateur)); }

  onSubmit() {
    let ret: boolean = false;

    this.utilisateur.nom = this.inscriptionForm.controls.inputNom.value;
    this.utilisateur.prenom = this.inscriptionForm.controls.inputPrenom.value;
    this.utilisateur.email = this.inscriptionForm.controls.inputEmail.value;
    this.utilisateur.telephone = this.telephonePipe.transform(this.inscriptionForm.controls.inputTelephone.value);
    this.utilisateur.motDePasse = this.inscriptionForm.controls.inputMotDePasse.value;
    this.utilisateur.confirmation = this.inscriptionForm.controls.inputConfirmation.value;

    this.reponse = this.echangeService.CreerUtilisateur(this.utilisateur);
    this.reponse.subscribe(res => {
      if(res === 1) {
        this.AddUtilisateur(this.utilisateur);

        this.router.navigate(['/Connexion/Inscription/Recapitulatif']);

        ret = true;
      }
      else {
        alert("Email déjà existant, veuillez en choisir un autre.");
        ret = false;
      }
    });

    return ret;
  }
}
