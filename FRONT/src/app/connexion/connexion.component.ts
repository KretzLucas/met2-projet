import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EchangeService } from '../echange.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.less']
})
export class ConnexionComponent implements OnInit {
  inputEMail: string;
  inputMotDePasse: string;
  reponse: any;

  constructor(private echangeService: EchangeService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    let ret: boolean = false;
    this.reponse = this.echangeService.Login(this.inputEMail, this.inputMotDePasse);
    this.reponse.subscribe(res => {
      if(res.token != null) {
        sessionStorage.setItem('token', res.token);
        this.router.navigate(['/Catalogue']);
        ret = true;
      }
      else {
        alert("Connexion échouée, l'email ou le mot de passe est incorrect.");
        ret = false;
      }
    });
    return ret;
  }
}
