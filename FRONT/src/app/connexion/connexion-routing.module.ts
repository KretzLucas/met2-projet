import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { RecapitulatifComponent } from './inscription/recapitulatif/recapitulatif.component';

const routes: Routes = [
  {path:'', component:ConnexionComponent},
  {path:'Inscription', component:InscriptionComponent},
  {path:'Inscription/Recapitulatif', component:RecapitulatifComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConnexionRoutingModule { }
