# Environnement

* OS : macOS Catalina 10.15.2
* Navigateur : Safari
* IDE : Visual Studio Code
* Base de données : MySQL
* Serveur : php

# Installation & Acquisition du projet

* Ouvrir un terminal
* Se situer dans le dossier qui comportera le projet
* Lancer la commande : git clone https://gitlab.com/KretzLucas/met2-projet.git
* cd Projet/
* npm install @auth0/angular-jwt
* Installer MySQL :
* * Lien mac (premier lien) : https://dev.mysql.com/downloads/mysql/
* * Sur linux, lancer la commande : apt-get install mysql-server mysql-client mysql-common
* Se connecter en tant que root et lancer le script : script.sql
* Vérifier le port de la base avec la requête SQL : SHOW VARIABLES WHERE Variable_name = 'port';
* Vérifier la socket de MySQL avce la requête SQL : SHOW VARIABLES WHERE Variable_name = 'socket';
* Si, dans le fichier bootstrap.php, les valeurs sont différentes, les changer par les résultats des requêtes

# Lancement du projet

Dans le dossier racine du projet :

* Ouvrir deux terminals
* Sur le premier terminal, exécuter les commandes suivantes :
* * cd FRONT/
* * ng serve --proxy-config proxy.conf.json
* Sur le second terminal, exécuter les commandes suivantes :
* * cd BACK/src/
* * php -S localhost:8080

# Commandes exécutées en local pour installer le projet

## Composer

Dans le dossier racine du projet :

* cd BACK/
* php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
* php -r "if (hash_file('sha384', 'composer-setup.php') === 'c5b9b6d368201a9db6f74e2611495f369991b72d9c8cbd3ffbc63edff210eb73d46ffbfce88669ad33695ef77dc76976') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
* php composer-setup.php
* php -r "unlink('composer-setup.php');"

## Slim v.3

Dans le dossier racine du projet :

* cd BACK/
* php composer.phar require slim/slim:"3.*"

## JWT

Dans le dossier racine du projet :

* cd BACK/
* php composer.phar require firebase/php-jwt
* php composer.phar require tuupola/slim-jwt-auth

## Doctrine

Dans le dossier racine du projet :

* cd BACK/
* touch bootstrap.php : ajout du code du cours, légère modification de la varibale config et de la variable $conn :
* * $config = Setup::createYAMLMetadataConfiguration(array(__DIR__ . "/config/yaml"), $isDevMode);
* * $conn = array(
        'driver' => 'pdo_mysql',
        'user' => 'user',
        'password' => '',
        'dbname' => 'commerce',
        'port' => '3306',
        'unix_socket' => '/tmp/mysql.sock'
    );
* touch cli-config.php : ajout du code du cours
* Modification du composer.json en ajoutant :
    "doctrine/orm" : "2.*",
            "symfony/yaml":"*"
        },
        "autoload": {
            "classmap":["src/models/"]
        }
* php composer.phar update
* php vendor/bin/doctrine orm:convert-mapping --namespace="" --force --from-database yml ./config/yaml
* php vendor/bin/doctrine orm:generate-entities --generate-annotations=false --update-entities=true --generate-methods=false ./src/models
* php vendor/bin/doctrine orm:schema-tool:update --force
* php vendor/bin/doctrine orm:validate-schema
* php vendor/bin/doctrine orm:clear-cache:metadata