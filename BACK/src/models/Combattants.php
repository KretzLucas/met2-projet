<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Combattants
 *
 * @ORM\Table(name="Combattants")
 * @ORM\Entity
 */
class Combattants
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    public $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=100, nullable=false)
     */
    public $prenom;

    /**
     * @var int
     *
     * @ORM\Column(name="naissance", type="integer", nullable=false)
     */
    public $naissance;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=false)
     */
    public $poids;

    /**
     * @var float
     *
     * @ORM\Column(name="taille", type="float", precision=10, scale=0, nullable=false)
     */
    public $taille;

    /**
     * @var string
     *
     * @ORM\Column(name="sport", type="string", length=20, nullable=false)
     */
    public $sport;

    /**
     * @var string
     *
     * @ORM\Column(name="pointFort", type="string", length=40, nullable=false)
     */
    public $pointfort;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=200, nullable=false)
     */
    public $image;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    public $prix;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Combattants
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return Combattants
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set naissance.
     *
     * @param int $naissance
     *
     * @return Combattants
     */
    public function setNaissance($naissance)
    {
        $this->naissance = $naissance;

        return $this;
    }

    /**
     * Get naissance.
     *
     * @return int
     */
    public function getNaissance()
    {
        return $this->naissance;
    }

    /**
     * Set poids.
     *
     * @param int $poids
     *
     * @return Combattants
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids.
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set taille.
     *
     * @param float $taille
     *
     * @return Combattants
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * Get taille.
     *
     * @return float
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Set sport.
     *
     * @param string $sport
     *
     * @return Combattants
     */
    public function setSport($sport)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport.
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set pointfort.
     *
     * @param string $pointfort
     *
     * @return Combattants
     */
    public function setPointfort($pointfort)
    {
        $this->pointfort = $pointfort;

        return $this;
    }

    /**
     * Get pointfort.
     *
     * @return string
     */
    public function getPointfort()
    {
        return $this->pointfort;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Combattants
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set prix.
     *
     * @param float $prix
     *
     * @return Combattants
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix.
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }
}
