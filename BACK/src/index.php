<?php

require '../vendor/autoload.php';
require '../bootstrap.php';

use \Firebase\JWT\JWT;

$app = new \Slim\App;

const JWT_SECRET = "MaCle6768";

$jwt = new \Tuupola\Middleware\JwtAuthentication([
    "path" => "/api",
    "secure" => false,
    "ignore" => ["/api/login", "/api/client"],
    "secret" => JWT_SECRET,
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {  
        $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

$app->add($jwt);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, X-Requested-With, Observe');
header('Access-Control-Expose-Headers: Authorization');
header('Content-Type: application/json');

$app->get('/api/clients', 'getClients');
$app->get('/api/clients/{id}', 'getClient');
$app->post('/api/client', 'addClient');

$app->get('/api/produits', 'getProduits');
$app->get('/api/produits/{id}', 'getProduit');

$app->post('/api/login', 'login');

//---------- Début partie client ----------

function getClients($request, $response, $args) {
    global $entityManager;

    $utilisateurRepository = $entityManager->getRepository('Utilisateurs');
    $utilisateurs = $utilisateurRepository->findAll();

    return $response->write(json_encode($utilisateurs));
}

function getClient($request, $response, $args) {
    $id = $args['id'];

    global $entityManager;

    $utilisateurRepository = $entityManager->getRepository('Utilisateurs');
    $utilisateur = $utilisateurRepository->findOneById($id);
    
    return $response->write(json_encode($utilisateur));
}

function addClient($request, $response, $args) {
    $body = $request->getParsedBody();

    global $entityManager;

    $utilisateurRepository = $entityManager->getRepository('Utilisateurs');

    $ajout = 0;
    
    if(!ClientExist($body['email'])) {
        $utilisateur = new Utilisateurs();
        $utilisateur->setNom($body['nom']);
        $utilisateur->setPrenom($body['prenom']);
        $utilisateur->setEmail($body['email']);
        $utilisateur->setTelephone($body['telephone']);
        $utilisateur->setMotDePasse($body['motDePasse']);
        $utilisateur->setConfirmation($body['confirmation']);
        $entityManager->persist($utilisateur);
        $entityManager->flush();
        $ajout = 1;
    }

    return $response->write($ajout);
}

function ClientExist($email) {
    global $entityManager;

    $utilisateurRepository = $entityManager->getRepository('Utilisateurs');
    $utilisateur = $utilisateurRepository->findOneByEmail($email);

    $exist = false;

    if($utilisateur != null) {
        $exist = true;
    }
    
    return $exist;
}

//---------- Fin partie client ----------

//---------- Début partie produit ----------

function getProduits($request, $response, $args) {
    global $entityManager;

    $combattanntRepository = $entityManager->getRepository('Combattants');
    $combattants = $combattanntRepository->findAll();
    
    return $response->write(json_encode($combattants));
}

function getProduit($request, $response, $args) {    
    $id = $args['id'];

    global $entityManager;

    $combattanntRepository = $entityManager->getRepository('Combattants');
    $combattants = $combattanntRepository->findOneById($id);
    
    return $response->write(json_encode($combattants));
}

//---------- Fin partie produit ----------

//---------- Début partie JWT ----------

function login ($request, $response, $args) {
    $body = $request->getParsedBody();

    global $entityManager;

    $utilisateurRepository = $entityManager->getRepository('Utilisateurs');
    $utilisateur = $utilisateurRepository->findOneBy(array('email' => $body['email'], 'motdepasse' => $body['motDePasse']));
    
    $exist = 0;

    if($utilisateur != null) {
        $exist = 1;
    }

    if($exist == 1) {
        $issuedAt = time();
        $expirationTime = $issuedAt + 60;
        $payload = array(
            'userid' => $userMail,
            'iat' => $issuedAt,
            'exp' => $expirationTime
        );
        
        $token = JWT::encode($payload, JWT_SECRET, "HS256");
        
        $response = $response->withHeader("Authorization", "Bearer {$token}")->withHeader("Content-Type", "application/json");
    }
    
    return $response->write(json_encode(array('token' => $token)));
}

//---------- Fin partie JWT ----------

$app->run(); 
?>